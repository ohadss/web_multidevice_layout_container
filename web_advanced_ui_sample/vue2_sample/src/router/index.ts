import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import HomeView from "../views/HomeView.vue";
import DetailView from "../views/DetailView.vue";
import ClassifyView from "../views/ClassifyView.vue";
import DiscoveryView from "../views/DiscoveryView.vue";
import MineView from "../views/MineView.vue";
import ShoppingBagView from "../views/ShoppingBagView.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/detail_pay",
    name: "DetailView",
    component: DetailView,
  },
  {
    path: "/classify",
    name: "classify",
    component: ClassifyView,
  },
  {
    path: "/discovery",
    name: "DiscoveryView",
    component: DiscoveryView,
  },
  {
    path: "/mine",
    name: "MineView",
    component: MineView,
  },
  {
    path: "/shoppingBag",
    name: "ShoppingBagView",
    component: ShoppingBagView,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
