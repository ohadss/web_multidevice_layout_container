import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

const i18n: any = new VueI18n({
    locale: localStorage.getItem("lang") || "zh",
    messages: {
        zh: {
            tabs: {
                firstPage: "首页",
                classify: "分类",
                discovery: "发现",
                shoppingBag: "购物袋",
                mine: "我的",
            },
            categories: {
                smartOffice: "智慧办公",
                smartHome: "智慧家居",
                huaweiPhone: "华为手机",
                entertainment: "影音娱乐",
                sportsHealth: "运动健康",
                aitoCar: "AITO汽车",
                huaweiSmartSelection: "华为智选",
                hongmengZhilian: "鸿蒙智联",
                enterpriseCommercial: "企业商用",
                wholeHouseIntelligence: "全屋智能",
            }
        },
        en: {
            tabs: {
                firstPage: "First Page",
                classify: "Classify",
                discovery: "Discover",
                shoppingBag: "Shopping Bag",
                mine: "Mine",
            },
            categories: {
                smartOffice: "Smart Office",
                smartHome: "Smart Home",
                huaweiPhone: "Huawei Phone",
                entertainment: "Entertainment",
                sportsHealth: "Sports Health",
                aitoCar: "AITO Car",
                huaweiSmartSelection: "Huawei Smart Selection",
                hongmengZhilian: "Hongmeng Zhilian",
                enterpriseCommercial: "Enterprise Commercial",
                wholeHouseIntelligence: "Whole House Intelligence",
            }
        }
    }
});

export default i18n;