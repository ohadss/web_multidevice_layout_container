# vue2_sample

#### 介绍
这是一个使用vue2语言完成的一个sample，里面使用了[web_multidevice_advanced_ui](https://gitee.com/ohadss/web_multidevice_advanced_ui) 库里的自定义组件来完成导航栏、网格布局、类挪移布局等布局方式，并实现了响应式布局，让sample在手机、折叠屏、平板上都有一个良好的表现效果。

#### 效果预览

![pingban](resource/Matepad_Pro_13.2.png)

![zhedieping](resource/Mate_X5.png)

![zhiban](resource/mate60.png)

##  工程目录

```
web_advanced_ui_sample  
├─vue2_sample  // 项目根目录
│  ├─src  
│  │  ├─assets  // 图片资源文件夹
│  │  ├─components // 组件文件夹  
│  │  │  ├─Categories // 功能入口组件，使用网格组件完成
│  │  │  ├─PageHeader // 页面头部，使用类挪移布局完成 
│  │  │  ├─DetailChart // 商品列表页静态图片 
│  │  │  ├─DetailHeader // 商品列表页头部 
│  │  │  ├─DetailList // 商品列表页商品列表 
│  │  │  ├─PageHeader // 页面头部，使用类挪移布局完成 
│  │  │  ├─RecommendedArea // 页面下半部，使用类挪移布局完成 
│  │  │  └─TabBar // 导航栏，使用导航栏组件完成
│  │  │ 
│  │  ├─i18n // 国际化配置
│  │  ├─router // 路由
│  │  ├─store
│  │  ├─utils // 公共工具  
│  │  │   breakpointInit.ts  // 断点公共方法
│  │  ├─views // 页面文件夹
│  │  │    HomeView.vue  // 首页
│  │  ├─App.vue  // 首页入口
│  │  ├─main.ts  // 项目配置
```

##  具体实现

1.  本sample使用vue2开发完成，使用了库[web_multidevice_advanced_ui](https://gitee.com/ohadss/web_multidevice_advanced_ui) 里的导航栏、网格布局、类挪移布局等组件以及断点封装管理来实现响应式布局。

#### 使用说明

前置操作：

1. 在vue2_sample项目的文件夹下打开cmd，运行npm i安装依赖。

2. 在vue2_sample项目的文件夹下打开cmd，运行npm run serve，启动vue项目。

本sample使用有两种方式，取其一即可。

1. 使用浏览器访问页面。
   
   直接在浏览器里输入项目启动后，控制台中提示的url即可访问。

2. 使用鸿蒙项目加载访问页面（需获取网络请求权限）。

   鸿蒙项目加载页面需确保项目运行的机器（如：手机，折叠屏、平板）与项目服务启动的机器处在同一个局域网中。

   使用如下代码加载页面（示例中的属性按需添加，url需改为vue项目启动时控制台提示的Network对应的url）。

   ```
   Web({ src: 'http://192.168.43.4:8080', controller: this.webController })
     .enableNativeEmbedMode(true)
     .domStorageAccess(true)
     .multiWindowAccess(true)
     .javaScriptAccess(true)
     .geolocationAccess(true)
     .imageAccess(true)
     .onlineImageAccess(true)
     .fileAccess(true)
     .mediaPlayGestureAccess(true)
     .mixedMode(MixedMode.Compatible)
     .height('100%')
     .width('100%')
   ```

##  相关权限

1.  使用鸿蒙项目加载访问页面时，需获取网络请求权限：ohos.permission.INTERNET。

##  约束与限制

1.  本示例仅支持标准系统上运行，支持设备：直板机、折叠机、平板、pc。
2.  HarmonyOS系统：HarmonyOS NEXT Developer Beta5及以上。
3.  DevEco Studio版本：DevEco Studio NEXT Developer Beta5及以上。
4.  HarmonyOS SDK版本：HarmonyOS NEXT Developer Beta5 SDK及以上。
