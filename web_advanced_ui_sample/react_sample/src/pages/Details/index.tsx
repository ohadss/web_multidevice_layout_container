import React, { Component } from 'react';
import DetailsHeader from '../../components/DetailsHeader';
import DetailsChart from '../../components/DetailsChart';
import DetailsList from '../../components/DetailsList';
import './index.css';

class Details extends Component {
  state = {
    newListData: [
      { id: 1, name: "Mate 60 Pro+", imgSrc: "./img/classify_product_1.png" },
      { id: 2, name: "Mate 60", imgSrc: "./img/classify_product_2.png" },
      { id: 3, name: "Mate 60 Pro", imgSrc: "./img/classify_product_3.png" },
      { id: 4, name: "Mate X5", imgSrc: "./img/classify_product_4.png" },
    ],
    mateListData: [
      { id: 1, name: "Mate 60 Pro+", imgSrc: "./img/classify_product_1.png" },
      { id: 2, name: "Mate 60", imgSrc: "./img/classify_product_2.png" },
      { id: 3, name: "Mate 60 Pro", imgSrc: "./img/classify_product_3.png" },
      { id: 4, name: "Mate X5", imgSrc: "./img/classify_product_4.png" },
      { id: 5, name: "Mate X3", imgSrc: "./img/classify_product_5.png" },
      { id: 6, name: "Mate 50", imgSrc: "./img/classify_product_6.png" },
      { id: 7, name: "Mate 50 Pro", imgSrc: "./img/classify_product_7.png" },
      { id: 8, name: "Mate Xs 2", imgSrc: "./img/classify_product_8.png" },
      { id: 9, name: "Mate 50E", imgSrc: "./img/classify_product_9.png" },
      { id: 10, name: "Mate 50 RS", imgSrc: "./img/classify_product_10.png" },
    ],
    puraListData: [
      { id: 1, name: "P60", imgSrc: "./img/classify_product_11.png" },
      { id: 2, name: "P60 Pro", imgSrc: "./img/classify_product_12.png" },
      { id: 3, name: "P60 Art", imgSrc: "./img/classify_product_13.png" },
      { id: 4, name: "P50 Pro", imgSrc: "./img/classify_product_14.png" },
      { id: 5, name: "P50E", imgSrc: "./img/classify_product_15.png" },
      { id: 6, name: "P50 Pocket", imgSrc: "./img/classify_product_16.png" },
    ],
    newListTitle: '新品上市',
    mateListTitle: 'HUAWEI Mate系列',
    puraListTitle: 'HUAWEI P系列',
  };

  render() {
    const { newListData, mateListData, puraListData, newListTitle, mateListTitle, puraListTitle } = this.state;
    return(
      <div className="details-page">
        <div className='details-header-area'>
          <DetailsHeader></DetailsHeader>
        </div>
        <div className='details-chart-area'>
          <DetailsChart></DetailsChart>
        </div>
        <div className='details-list-area'>
          <DetailsList details-list-data={newListData}
                       details-list-title={newListTitle}></DetailsList>
          <DetailsList details-list-data={mateListData}
                       details-list-title={mateListTitle}></DetailsList>
          <DetailsList details-list-data={puraListData}
                       details-list-title={puraListTitle}></DetailsList>
        </div>
      </div>
    )
  }
}

export default Details;