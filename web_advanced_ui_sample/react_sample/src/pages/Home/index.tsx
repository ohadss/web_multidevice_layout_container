import React, { Component } from 'react';
import PageHeader from '../../components/PageHeader';
import Categories from '../../components/Categories';
import RecommendedArea from '../../components/RecommendedArea';
import './index.css';

class Home extends Component {
  constructor(props: any) {
    super(props);
  }

  render() {
    return(
      <div className="first-page">
        <PageHeader></PageHeader>
        <div className="context">
          <Categories></Categories>
        </div>
        <div className="part">
          <RecommendedArea></RecommendedArea>
        </div>
      </div>
    )
  }
}

export default Home;