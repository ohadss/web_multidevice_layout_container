import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import zh_CN from './zh_CN';
import en_US from './en_US';

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    debug: true,
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        translation: {
          ...en_US,
        }
      },
      zh: {
        translation: {
          ...zh_CN,
        }
      }
    }
  });

export default i18n;