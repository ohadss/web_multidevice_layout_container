import { BreakpointManager } from "web-multidevice-advanced-ui/dist/hookInput";

export const breakpointManager = new BreakpointManager({
  xs: 0,
  sm: 320,
  md: 600,
  lg: 840,
  xl: 1440,
});