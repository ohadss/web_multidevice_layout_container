import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

export const withRouter = (Child) => {
  return (props) => {
    const navigate = useNavigate();
    return <Child {...props} navigate={navigate} ></Child>
  }
}

export const withTranslation = (Child) => {
  return (props) => {
    const { t, i18n } = useTranslation();
    return <Child {...props} trans={t} i18n={i18n} ></Child>
  }
}