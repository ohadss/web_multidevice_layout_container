import React, { Component } from 'react';
import { HashRouter, Routes, Route } from 'react-router-dom';
import { breakpointManager } from './utils/breakpointManage';
import Home from './pages/Home';
import WithNavigate from './components/TabBarArea';
import Classification from './pages/Classification';
import Discovery from './pages/Discovery';
import ShoppingBag from './pages/ShoppingBag';
import Mine from './pages/Mine';
import Details from './pages/Details';
import './App.css';

class App extends Component {
  state = {
    marginLeft: '0',
    marginBottom: '0',
    breakpointManager,
    unsubscribe: (): void => {},
  }

  componentDidMount(): void {
    const unsubscribeMethod = this.state.breakpointManager.subscribeToBreakpoint(() => {
      this.setState({
        marginLeft: this.state.breakpointManager.useBreakpointValue({
          xs: 0,
          sm: 0,
          md: 0,
          lg: '96px',
          xl: '96px',
        }),
        marginBottom: this.state.breakpointManager.useBreakpointValue({
          xs: '78px',
          sm: '78px',
          md: '78px',
          lg: 0,
          xl: 0,
        }),
      }, () => {
        document.documentElement.style.setProperty('--marginLeft', this.state.marginLeft);
        document.documentElement.style.setProperty('--marginBottom', this.state.marginBottom);
      });
    });

    this.setState({
      unsubscribe: unsubscribeMethod
    });
  }

  componentWillUnmount(): void {
    this.state.unsubscribe();
  }

  render() {
    return (
      <HashRouter>
        <div className='App'>
          <WithNavigate></WithNavigate>
          <div className='template-main'>
            <Routes>
              <Route path='/' element={<Home />}></Route>
              <Route path='/classification' element={<Classification />}></Route>
              <Route path='/discovery' element={<Discovery />}></Route>
              <Route path='/shoppingBag' element={<ShoppingBag />}></Route>
              <Route path='/mine' element={<Mine />}></Route>
              <Route path='/details' element={<Details />}></Route>
            </Routes>
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default App;