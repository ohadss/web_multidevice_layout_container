import React, { Component } from 'react';
import { MultiGridReact, GridItemReact } from 'web-multidevice-advanced-react/dist/index';
import { breakpointManager } from '../../utils/breakpointManage';
import { withRouter, withTranslation } from '../../utils/functionProcess';
import './index.css';

class Categories extends Component {
  state = {
    columnsTemplate: '1fr 1fr 1fr 1fr 1fr',
    categoriesItems: [
      { id: 1, name: (this.props as any).trans('categories_smartOffice'), imgSrc: "./img/categories_1.png" },
      { id: 2, name: (this.props as any).trans('categories_huaweiPhone'), imgSrc: "./img/categories_2.png" },
      { id: 3, name: (this.props as any).trans('categories_sportsHealth'), imgSrc: "./img/categories_3.png" },
      { id: 4, name: (this.props as any).trans('categories_huaweiSmartSelection'), imgSrc: "./img/categories_4.png" },
      { id: 5, name: (this.props as any).trans('categories_enterpriseCommercial'), imgSrc: "./img/categories_5.png" },
      { id: 6, name: (this.props as any).trans('categories_smartHome'), imgSrc: "./img/categories_6.png" },
      { id: 7, name: (this.props as any).trans('categories_entertainment'), imgSrc: "./img/categories_7.png" },
      { id: 8, name: (this.props as any).trans('categories_aitoCar'), imgSrc: "./img/categories_8.png" },
      { id: 9, name: (this.props as any).trans('categories_harmonyLink'), imgSrc: "./img/categories_9.png" },
      { id: 10, name: (this.props as any).trans('categories_wholeHouseIntelligence'), imgSrc: "./img/categories_10.png" },
    ],
    breakpointManager,
    unsubscribe: (): void => {},
  };

  componentDidMount(): void {
    const unsubscribeMethod = this.state.breakpointManager.subscribeToBreakpoint(() => {
      this.setState({
        columnsTemplate: this.state.breakpointManager.useBreakpointValue({
          xs: "1fr 1fr 1fr 1fr 1fr",
          sm: "1fr 1fr 1fr 1fr 1fr",
          md: "1fr 1fr 1fr 1fr 1fr 1fr 1fr",
          lg: "1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr",
          xl: "1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr",
        }),
      })
    });

    this.setState({
      unsubscribe: unsubscribeMethod
    });
  }

  componentWillUnmount(): void {
    this.state.unsubscribe();
  }

  render() {
    const { columnsTemplate, categoriesItems } = this.state;
    const gridItemClick = () => {
      (this.props as any).navigate('/details');
    };
    return(
      <div className='categories'>
        <MultiGridReact columnsTemplate={columnsTemplate}
                        gridRowGap='10px'
                        gridColumnGap='10px'
                        onGridItemClick={gridItemClick}>
          {
            categoriesItems.map(item => (
              <GridItemReact key={item.id}>
                <div className='categories-item'>
                  <img src={item.imgSrc} />
                  <span>{ item.name }</span>
                </div>
              </GridItemReact>
            ))
          }
        </MultiGridReact>
      </div>
    )
  }
}

export default withTranslation(withRouter(Categories));