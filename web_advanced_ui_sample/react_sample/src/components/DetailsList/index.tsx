import React, { Component } from 'react';
import { MultiGridReact, GridItemReact } from 'web-multidevice-advanced-react/dist/index';
import { breakpointManager } from '../../utils/breakpointManage';
import './index.css';

class DetailsList extends Component {
  state = {
    detailsListTitle: this.props['details-list-title'],
    columnsTemplate: '1fr 1fr 1fr 1fr',
    detailsListItems: this.props['details-list-data'],
    breakpointManager,
    unsubscribe: (): void => {},
  };

  componentDidMount(): void {
    const unsubscribeMethod = this.state.breakpointManager.subscribeToBreakpoint(() => {
      this.setState({
        columnsTemplate: this.state.breakpointManager.useBreakpointValue({
          xs: "1fr 1fr 1fr 1fr",
          sm: "1fr 1fr 1fr 1fr",
          md: "1fr 1fr 1fr 1fr 1fr 1fr 1fr",
          lg: "1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr",
          xl: "1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr",
        }),
      })
    });

    this.setState({
      unsubscribe: unsubscribeMethod
    });
  }

  componentWillUnmount(): void {
    this.state.unsubscribe();
  }

  render() {
    const { columnsTemplate, detailsListItems, detailsListTitle } = this.state;
    return(
      <div className='details-list'>
        <div className='list-title'>{ detailsListTitle }</div>
        <MultiGridReact columnsTemplate={columnsTemplate}
                        gridRowGap='15px'
                        gridColumnGap='10px'>
          {
            detailsListItems.map(item => (
              <GridItemReact key={item.id}>
                <div className='details-list-item'>
                  <img src={item.imgSrc} />
                  <div>{ item.name }</div>
                </div>
              </GridItemReact>
            ))
          }
        </MultiGridReact>
      </div>
    )
  }
}

export default DetailsList;