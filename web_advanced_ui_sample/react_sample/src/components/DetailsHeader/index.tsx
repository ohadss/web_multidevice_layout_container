import React, { Component } from 'react';
import { withRouter } from '../../utils/functionProcess';
import './index.css';

class DetailsHeader extends Component {
  render() {
    const backClick = () => {
      (this.props as any).navigate('/')
    }
    return(
      <div className='details-header'>
        <img src="./img/back.png" 
             alt=""
             onClick={backClick} />
        <div className="details-search-bar">
          <input type="search-bar-text" 
                 className="details-input" 
                 placeholder="搜索..." />
          <img className="search-bar-image" 
                src="./img/msg_big.png" />
        </div>
      </div>
    )
  }
}

export default withRouter(DetailsHeader);