import React, { Component } from 'react';
import { TabBarReact, TabBarItemReact } from 'web-multidevice-advanced-react/dist/index';
import { breakpointManager } from '../../utils/breakpointManage';
import { withRouter, withTranslation } from '../../utils/functionProcess';
import './index.css';

class TabBarArea extends Component {
  state = {
    vertical: false,
    selectedBgColor: '#f1f3f5',
    defaultBgColor: '#f1f3f5',
    barWidth: '100%',
    barHegiht: '100px',
    selectedColor: '#d20a2c',
    layoutMode: 'vertical',
    tabItems: [
      { key: "first", name: (this.props as any).trans('tabs_firstPage'), urlName: "/", imgSrc: "./img/tab_home_selected.svg" },
      { key: "second", name: (this.props as any).trans('tabs_classify'), urlName: "/classification", imgSrc: "./img/tab_classification.svg" },
      { key: "third", name: (this.props as any).trans('tabs_discovery'), urlName: "/discovery", imgSrc: "./img/tab_discovery.svg" },
      { key: "forth", name: (this.props as any).trans('tabs_shoppingBag'), urlName: "/shoppingBag", imgSrc: "./img/tab_shopping_bag.svg" },
      { key: "fivth", name: (this.props as any).trans('tabs_mine'), urlName: "/mine", imgSrc: "./img/tab_mine.svg" },
    ],
    breakpointManager,
    unsubscribe: (): void => {},
  };

  componentDidMount(): void {
    const unsubscribeMethod = this.state.breakpointManager.subscribeToBreakpoint(() => {
      this.setState({
        vertical: this.state.breakpointManager.useBreakpointValue({
          xs: false,
          sm: false,
          md: false,
          lg: true, 
          xl: true,
        }),
        barWidth: this.state.breakpointManager.useBreakpointValue({
          xs: "100%",
          sm: "100%",
          md: "100%",
          lg: "98px",
          xl: "98px",
        }),
        barHegiht: this.state.breakpointManager.useBreakpointValue({
          xs: "96px",
          sm: "96px",
          md: "96px",
          lg: "100%",
          xl: "100%",
        }),
      })
    });

    this.setState({
      unsubscribe: unsubscribeMethod
    });
  }

  componentWillUnmount(): void {
    this.state.unsubscribe();
  }

  render() {
    const { vertical, selectedBgColor, defaultBgColor, barWidth, barHegiht, selectedColor, layoutMode, tabItems } = this.state;
    const tabItemClick = ({index}) => {
      const tabItems = this.state.tabItems;
      tabItems.forEach((item) => {
        item.imgSrc = item.imgSrc.replace("_selected.svg", ".svg");
      });
      tabItems[index].imgSrc = tabItems[index].imgSrc.replace(
        ".svg",
        "_selected.svg"
      );
      this.setState({
        tabItems
      });
      (this.props as any).navigate(tabItems[index].urlName)
    };
    
    return(
      <div className='tab-bar-area'>
        <TabBarReact selectedColor={selectedColor}
                     vertical={vertical}
                     selectedBgColor={selectedBgColor}
                     bgColor={defaultBgColor}
                     width={barWidth}
                     height={barHegiht}
                     layoutMode={layoutMode}
                     onTabItemClick={tabItemClick}>
          {
            tabItems.map(item => (
              <TabBarItemReact key={item.key}>
                  <img src={item.imgSrc} />
                  <span>{ item.name }</span>
              </TabBarItemReact>
            ))
          }
        </TabBarReact>
      </div>
    )
  }
}

export default withTranslation(withRouter(TabBarArea));