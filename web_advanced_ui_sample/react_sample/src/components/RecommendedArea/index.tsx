import React, { Component } from 'react';
import { MultiDiversionReact, DiversionItemReact } from 'web-multidevice-advanced-react/dist/index';
import { breakpointManager } from '../../utils/breakpointManage';
import './index.css';

class RecommendedArea extends Component {
  state = {
    multiDiversionDirection: 'vertical',
    contentCols: '6',
    gapStyleBottom: '15px',
    gapStyleRight: '0px',
    gapStyleLeft: '0px',
    breakpointManager,
    unsubscribe: (): void => {},
  };

  componentDidMount(): void {
    const unsubscribeMethod = this.state.breakpointManager.subscribeToBreakpoint(() => {
      this.setState({
        multiDiversionDirection: this.state.breakpointManager.useBreakpointValue({
          xs: "vertical",
          sm: "vertical",
          md: "horizontal",
          lg: "horizontal",
          xl: "horizontal",
        }),
        contentCols: this.state.breakpointManager.useBreakpointValue({
          xs: "12",
          sm: "12",
          md: "6",
          lg: "6",
          xl: "6",
        }),
        gapStyleBottom: this.state.breakpointManager.useBreakpointValue({
          xs: "10px",
          sm: "10px",
          md: "0px",
          lg: "0px",
          xl: "0px",
        }),
        gapStyleRight: this.state.breakpointManager.useBreakpointValue({
          xs: "0px",
          sm: "0px",
          md: "15px",
          lg: "15px",
          xl: "15px",
        }),
        gapStyleLeft: this.state.breakpointManager.useBreakpointValue({
          xs: "0px",
          sm: "0px",
          md: "10px",
          lg: "10px",
          xl: "10px",
        }),
      })
    });

    this.setState({
      unsubscribe: unsubscribeMethod
    });
  }

  componentWillUnmount(): void {
    this.state.unsubscribe();
  }

  render() {
    const { multiDiversionDirection, contentCols, gapStyleBottom, gapStyleRight, gapStyleLeft } = this.state;
    return(
      <MultiDiversionReact direction={multiDiversionDirection}>
        <DiversionItemReact name="firstPart"
                            diversionCols={contentCols}>
          <div className="recommended-product-view" style={{marginBottom: gapStyleBottom, marginRight: gapStyleRight}}>
            <img className="recommended-product-view-main"
                src="./img/recommended_product_1.png" />
            <img className="recommended-product-view-overlay" 
                src="./img/icon_video.png" />
          </div>
        </DiversionItemReact>
        <DiversionItemReact name="secondPart"
                            diversionCols={contentCols}>
          <div className="second-view" style={{marginLeft: gapStyleLeft}}>
            <div className="welfare-view">
              <div className="welfare-view-title">
                <span className="welfare-view-title1">福利专区</span>
                <span className="welfare-view-title2">立即领券</span>
              </div>
              <div className="welfare-view-context">
                <div className="welfare-view-context-child">
                  <img className="welfare-view-context-child1" src="./img/welfare_product_1.png" />
                  <span className="welfare-view-context-child2">用券价</span>
                  <span className="welfare-view-context-child3">￥1299起</span>
                </div>
                <div className="welfare-view-context-child">
                  <img className="welfare-view-context-child1" src="./img/categories_9.png" />
                  <span className="welfare-view-context-child2">用券价</span>
                  <span className="welfare-view-context-child3">￥899起</span>
                </div>
                <div className="welfare-view-context-child">
                  <img className="welfare-view-context-child1" src="./img/welfare_product_5.png" />
                  <span className="welfare-view-context-child2">用券价</span>
                  <span className="welfare-view-context-child3">￥599起</span>
                </div>
              </div>
            </div>
            <div className="selection">
              <div className="selection-title">
                <span>甄选推荐</span>
                <div>更多
                  <img src="./img/ic_arrow_right_grey.png" />
                </div>
              </div>
              <div className="selection-context">
                <div className="selection-context-child">
                  <img className="selection-context-child1" 
                      src="./img/section_product_1.png" />
                  <span className="selection-context-child2">MateBook X Pro 12代</span>
                  <div className="selection-context-child3"></div>
                  <span className="selection-context-child4">V1-V5优惠400元</span>
                  <span className="selection-context-child5">￥10999</span>
                  <span className="selection-context-child6">2571人评论 95%好评</span>
                </div>
                <div className="selection-context-child">
                  <img className="selection-context-child1" 
                      src="./img/section_product_2.png" />
                  <span className="selection-context-child2">HUAWEI Mate50</span>
                  <span className="selection-context-child3">北斗卫星消息</span>
                  <span className="selection-context-child4">新一代直屏旗舰机</span>
                  <span className="selection-context-child5">￥10999</span>
                  <span className="selection-context-child6">2571人评论 95%好评</span>
                </div>
              </div>
            </div>
          </div>
        </DiversionItemReact>
      </MultiDiversionReact>
    )
  }
}

export default RecommendedArea;