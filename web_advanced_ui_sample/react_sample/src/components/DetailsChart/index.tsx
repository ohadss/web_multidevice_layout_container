import React, { Component } from 'react';
import { breakpointManager } from '../../utils/breakpointManage';
import './index.css';

class DetailsChart extends Component {
  state = {
    totalCharts: [
      { id: 1, name: "轮播图1", imgSrc: "./img/classify_banner1.png" },
      { id: 2, name: "轮播图2", imgSrc: "./img/classify_banner2.png" },
      { id: 3, name: "轮播图3", imgSrc: "./img/classify_banner3.png" },
    ],
    chartItems: 1,
    breakpointManager,
    unsubscribe: (): void => {},
  };

  componentDidMount(): void {
    const unsubscribeMethod = this.state.breakpointManager.subscribeToBreakpoint(() => {
      this.setState({
        chartItems: this.state.breakpointManager.useBreakpointValue({
          xs: 1,
          sm: 1,
          md: 2,
          lg: 3,
          xl: 3,
        }),
      })
    });

    this.setState({
      unsubscribe: unsubscribeMethod
    });
  }

  componentWillUnmount(): void {
    this.state.unsubscribe();
  }

  render() {
    const { totalCharts, chartItems } = this.state;

    return(
      <div className='details-chart'>
        {
          totalCharts.slice(0, chartItems).map(item => (
            <div className='chart-item'
                 key={item.id}>
              <img src={item.imgSrc} />
            </div>
          ))
        }
      </div>
    )
  }
}

export default DetailsChart;