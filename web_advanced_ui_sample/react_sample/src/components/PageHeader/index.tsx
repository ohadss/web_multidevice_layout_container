import React, { Component } from 'react';
import { MultiDiversionReact, DiversionItemReact } from 'web-multidevice-advanced-react/dist/index';
import { breakpointManager } from '../../utils/breakpointManage';
import './index.css';

class PageHeader extends Component {
  state = {
    multiDiversionDirection: 'vertical',
    tabBarCols: '7',
    searchBarCols: '5',
    barGap: '10px',
    breakpointManager,
    unsubscribe: (): void => {},
  };

  componentDidMount(): void {
    const unsubscribeMethod = this.state.breakpointManager.subscribeToBreakpoint(() => {
      this.setState({
        multiDiversionDirection: this.state.breakpointManager.useBreakpointValue({
          xs: "vertical",
          sm: "vertical",
          md: "horizontal",
          lg: "horizontal",
          xl: "horizontal",
        }),
        tabBarCols: this.state.breakpointManager.useBreakpointValue({
          xs: "12",
          sm: "12",
          md: "7",
          lg: "8",
          xl: "8",
        }),
        searchBarCols: this.state.breakpointManager.useBreakpointValue({
          xs: "12",
          sm: "12",
          md: "5",
          lg: "4",
          xl: "8",
        }),
      })
    });

    this.setState({
      unsubscribe: unsubscribeMethod
    });
  }

  componentWillUnmount(): void {
    this.state.unsubscribe();
  }

  render() {
    const { multiDiversionDirection, tabBarCols, searchBarCols, barGap } = this.state;
    const tabItemClick = () => {

    }
    return(
      <div className='home-header'>
        <MultiDiversionReact direction={multiDiversionDirection}>
          <DiversionItemReact name='firstTitle'
                              diversionCols={tabBarCols}
                              diversionGap={barGap}>
            <div className="top-tab-bar">
              <span className="top-tab-bar-text-selected">华为</span>
              <span className="top-tab-bar-text">AITO汽车</span>
              <span className="top-tab-bar-text">华为智选</span>
              <span className="top-tab-bar-text">生态周边</span>
            </div>
          </DiversionItemReact>
          <DiversionItemReact name='secondTitle'
                              diversionCols={searchBarCols}
                              diversionGap={barGap}>
            <div className="search-bar">
              <input type="search-bar-text" 
                     className="search-bar-input" 
                     placeholder="搜索..." />
              <img className="search-bar-image" 
                   src="./img/line_viewfinder.svg" />
              <img className="search-bar-image" 
                   src="./img/msg_big.png" />
            </div>
          </DiversionItemReact>
        </MultiDiversionReact>
      </div>
    )
  }
}

export default PageHeader;