# web_multidevice_layout_container

#### 介绍
This is an arkUI container solution that adapts to the multi-device layout of the HTML5 framework.

#### 效果预览

![alt text](resource/h5_sample.gif)

##  工程目录

```
web_multidevice_layout_container  
├─h5_sample  // ArkTS项目
│  ├─ets  
│  │  ├─common  // 常量文件夹
│  │  │      BreakpointConstants.ets  // 断点相关常量
│  │  │      CommonConstants.ets  // 一般常量
│  │  │      HomeConstants.ets  // 数据常量
│  │  │  
│  │  ├─entryability  
│  │  │      EntryAbility.ets
│  │  │ 
│  │  ├─entrybackupability
│  │  │      EntryBackupAbility.ets
│  │  │ 
│  │  ├─pages
│  │  │      Detail.ets  // H5页面
│  │  │      HomeContent.ets  // 首页页面
│  │  │      Index.ets  // 首页入口
│  │  │
│  │  ├─util 
│  │  │      BreakpointType.ets  // 断点相关工具
│  │  │      Logger.ets  // 日志类
│  │  │      ResourceUtil.ets  // 资源相关工具
│  │  │  
│  │  ├─view  
│  │  │      Categories.ets  // 首页分类视图
│  │  │      CommonView.ets  // 一些复用的组件
│  │  │      FlashSale.ets  // 限时秒杀区视图
│  │  │      HomeHeader.ets  // 首页头视图
│  │  │      RecommendedProductView.ets  // 直播视图
│  │  │      Selection.ets  // 甄选推荐视图
│  │  │      WelfareView.ets  // 福利专区视图
│  │  │
│  │  └─viewModel
│  │          FooterTabViewModel.ets  // 底部导航栏数据
│  │          IconInfoViewModel.ets  // 福利视图数据
│  │          RecommendedProductViewModel.ets  // 直播视图数据
│  │          SectionProductsViewModel.ets  // 推荐视图数据
│
└─vue_project // vue工程
   │  babel.config.js // 
   │  package.json // vue工程信息
   │  vue.config.js // vue工程服务信息
   └─src
   │  App.vue // 首页
   ├─assets // 资源文件夹
   ├─router 
   │      index.ts // 路由
   │
   └─views 
          DetailPay.vue // 详情页面1
          DetailView.vue // 详情页面2
```

##  具体实现

1.  本sample首页使用ArkTS实现，详情页面采用vue实现，并使用Web加载vue页面展示。首页调起详情页面采用的是自定义弹窗的方式，详情页面间的跳转采用的是vue的router实现的，详情页面1回退到首页使用的是[registerJavaScriptProxy](https://developer.huawei.com/consumer/cn/doc/harmonyos-references-V5/_ark_web___controller_a_p_i-V5#registerjavascriptproxy)的方式来实现的。

#### 使用说明

1. 本sample使用有两种方式，取其一即可。

   前置操作：在vue_project项目的文件夹下打开cmd，运行npm i安装依赖。

   1. 使用网络连接的方式访问h5页面。（需获取网络请求权限）
      1. 保证项目运行服务器与客户端处在同一个局域网内。
      
      2. 在vue_project项目的文件夹下打开cmd，运行npm run serve，启动vue项目。出现如下图示则说明项目启动ok（若端口有冲突需修改vue.config.js里的端口信息）。
      
         ![success.png](success.png)
      
      3. 修改h5_sample项目里的CommonConstants里的H5_URL为上图中Network对应的值。
      
      4. 启动h5_sample项目。
      
   2. 使用静态文件的方式访问h5页面。
      1. 在vue_project项目的文件夹下打开cmd，运行npm run build。会在vue_project文件夹下生成dist文件夹。
      2. 将dist文件夹拷贝到h5_sample项目里的resources下的rawfile文件夹下。
      3. 修改h5_sample项目里的Detail.ets。将web组件的src的值替换为 $rawfile('dist/index.html')。
      4. 启动h5_sample项目。

##  相关权限

1.  获取网络请求权限：ohos.permission.INTERNET。

##  约束与限制

1.  本示例仅支持标准系统上运行，支持设备：直板机、折叠机、平板、pc。
2.  HarmonyOS系统：HarmonyOS NEXT Developer Beta5及以上。
3.  DevEco Studio版本：DevEco Studio NEXT Developer Beta5及以上。
4.  HarmonyOS SDK版本：HarmonyOS NEXT Developer Beta5 SDK及以上。
