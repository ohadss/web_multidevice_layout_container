# web_multidevice_layout_container

#### 介绍
This is an arkUI container solution that adapts to the multi-device layout of the HTML5 framework.

##  工程目录

```
web_multidevice_layout_container 
├─ h5_sample // ArkTS首页+h5弹窗sample
|  ├─ h5_sample // ArkTS页面
|  └─ vue3_project // h5页面
├─ web_advanced_ui_sample  // 使用web_multidevice_advanced_ui库里组件实现的sample
   ├─ react_sample // 使用react实现的sample
   ├─ vue2_sample // 使用vue2实现的sample
   └─ vue3_sample // 使用vue3实现的sample
```

